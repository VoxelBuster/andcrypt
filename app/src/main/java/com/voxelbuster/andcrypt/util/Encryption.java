package com.voxelbuster.andcrypt.util;

import android.content.Context;
import android.security.keystore.KeyGenParameterSpec;
import androidx.security.crypto.EncryptedFile;
import androidx.security.crypto.MasterKeys;

import java.io.*;
import java.security.GeneralSecurityException;

public class Encryption {
    public static EncryptedFile encryptFile(File src, File dest, Context context) throws GeneralSecurityException, IOException {
        KeyGenParameterSpec keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC;
        String masterKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec);

        if (!dest.isFile()) {
            dest.createNewFile();
        }

        EncryptedFile encryptedFile =  new EncryptedFile.Builder(dest,
                context, masterKeyAlias, EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB).build();

        BufferedReader br = new BufferedReader(new FileReader(src));

        FileOutputStream fos = encryptedFile.openFileOutput();

        int readByte;
        while ((readByte = br.read()) > -1) {
            fos.write(readByte);
        }

        fos.close();
        return encryptedFile;
    }
}
